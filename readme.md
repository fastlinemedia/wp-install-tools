How To Use
==========

To use this call it in the folder you want to create a new wordpress install in

	bash wp-install-tools/wp-install-tools.sh

The script will ask for a directory name and install wordpress into that directory. It will also remove old twentyX themes, install a fresh _s theme, and rename the _s to FastLine