#!/bin/sh

#Install Wordpress
echo "Name of subdirectory:"
read subdirname
mkdir $subdirname
cd $subdirname

wget http://wordpress.org/latest.tar.gz
tar xfz latest.tar.gz
mv wordpress/* ./
rm latest.tar.gz
rmdir wordpress

#Download .gitignore file and initate git repo
wget -O .gitignore https://bitbucket.org/fastlinemedia/wp-install-tools/raw/master/gitignore.txt

#Remove Old Themes
cd wp-content/themes/
rm -rf twentyeleven/
rm -rf twentyten/


echo "Install Themes"
echo "[1] Install _s Theme"
echo "[2] Install Bootstrap Theme"
echo "[3] Install Foundation Theme"
echo "[4] Don't install a theme"
echo "Whats your choice?"
read whichTheme

if [ $whichTheme = "1" ]
then
   #Install _s theme
    git clone https://github.com/Automattic/_s.git FastLine_Theme
    cd FastLine_Theme

    #Remove git repo from _sls
    rm -rf .git/

    #Rename _s to FastLine_Theme
    find . -type f -print0 | xargs -0 perl -pi -e "s/\'_s\'/\'FastLine_Theme\'/g;"
    find . -type f -print0 | xargs -0 perl -pi -e "s/_s_/FastLine_Theme_/g;"
    find . -type f -print0 | xargs -0 perl -pi -e "s/_s\b/FastLine_theme/g;"
    find . -type f -print0 | xargs -0 perl -pi -e "s/Automattic\b/FastLine Media/g;"
    find . -type f -print0 | xargs -0 perl -pi -e "s/automattic.com\b/fastlinemedia.com/g;"
elif [ $whichTheme = "2" ]
then
    git clone https://github.com/320press/wordpress-bootstrap.git WordPress_Bootstrap
elif [ $whichTheme = "3" ]
then
    git clone https://github.com/320press/wordpress-foundation.git Foundation_Bootstrap
else
    echo 'WordPress installation compete'
fi

exit 0